package com.demo.springboot.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Service
public class SortService {
    public String getSorted(String digits){
        String sorted="";
        List<Integer> l = new ArrayList<>();
        char [] temp = digits.toCharArray();
        for(int i = 0; i < temp.length; i++){
            if(temp[i] == '-' && Character.isDigit(temp[i+1])){
                String po = "-";
                i++;
                while(i < temp.length && (Character.isDigit(temp[i]))){
                    po += temp[i++];
                }
                l.add(Integer.parseInt(po));
            }
            else if(Character.isDigit(temp[i]) ){
                String po = "";
                while(i < temp.length && (Character.isDigit(temp[i]))){
                    po += temp[i++];
                }
                l.add(Integer.parseInt(po));
            }
        }
        Collections.sort(l);
        for (int i = 0; i < l.size(); i++){
            sorted+=l.get(i);
            if(i != l.size()){
                sorted+=",";
            }
        }
        return sorted;
    }
}
