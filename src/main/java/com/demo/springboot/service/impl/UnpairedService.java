package com.demo.springboot.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class UnpairedService {
    public String getUnpaired(String digits) throws Exception {
        if (digits.isEmpty() || digits == null) {
            throw new Exception("Null");
        } else {
            String unpaired = "";
            List<Integer> l = new ArrayList<>();
            List<Integer> u = new ArrayList<>();
            char[] temp = digits.toCharArray();
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] == '-' && Character.isDigit(temp[i + 1])) {
                    String po = "-";
                    i++;
                    while (i < temp.length && (Character.isDigit(temp[i]))) {
                        po += temp[i++];
                    }
                    l.add(Integer.parseInt(po));
                } else if (Character.isDigit(temp[i])) {
                    String po = "";
                    while (i < temp.length && (Character.isDigit(temp[i]))) {
                        po += temp[i++];
                    }
                    l.add(Integer.parseInt(po));
                }
            }
            for (int i = 0; i < l.size(); i++) {
                if (l.get(i) % 2 != 0) {
                    if (u.contains(l.get(i))) {
                        continue;
                    } else {
                        u.add(l.get(i));
                    }
                }
            }
            for (int i = 0; i < u.size(); i++) {
                unpaired += u.get(i);
                if (i != u.size() - 1) {
                    unpaired += ",";
                }
            }

            return unpaired;
        }
    }
}
