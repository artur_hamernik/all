package com.demo.springboot.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class MaxService {
    public Double getMax(String listOfDigits) {
        Double max;
        List<Double> l = new ArrayList<Double>();
        char [] temp = listOfDigits.toCharArray();
        for(int i = 0; i < temp.length; i++){
            if(temp[i] == '-' && Character.isDigit(temp[i+1])){
                String po = "-";
                i++;
                while(i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i+1]))){
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
            else if(Character.isDigit(temp[i]) ){
                String po = "";
                while(i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i+1]))){
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
        }
        Collections.sort(l);
        max = l.get(l.size()-1);
        return max;
    }
}
