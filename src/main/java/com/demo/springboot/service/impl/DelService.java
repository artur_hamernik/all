package com.demo.springboot.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DelService {
    public String getLine(String line, char character){
        String temp ="";
        for(int i = 0; i < line.length(); i++){
            if(line.charAt(i) != Character.toUpperCase(character) && line.charAt(i) != Character.toLowerCase(character) ){
                temp+=line.charAt(i);
            }
        }
        return temp;
    }
}
