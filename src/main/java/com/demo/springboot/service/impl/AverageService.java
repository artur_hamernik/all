package com.demo.springboot.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AverageService {
    private List<Double> list = new ArrayList<Double>();
    public Double getAverage(String s){
        Double ave = 0.0;
        getNumbers(s);
        if(list.isEmpty()){
            System.out.println("Lista jest pusta podaj wartosci");
        }
        else{
            Double temp = 0.0;
            for (int i = 0; i < list.size(); i++){
                temp += list.get(i);
            }
            ave = temp/list.size();
        }
        return ave;
    }
    private void getNumbers(String s){
        List<Double> l = new ArrayList<Double>();
        char[] temp = s.toCharArray();
        for(int i = 0; i < temp.length; i++) {
            if (temp[i] == '-' && Character.isDigit(temp[i + 1])) {
                String po = "-";
                i++;
                while (i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i + 1]))) {
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            } else if (Character.isDigit(temp[i])) {
                String po = "";
                while (i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i + 1]))) {
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
        }
        list = l;
    }
}
