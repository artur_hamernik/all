package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.AverageData;
import com.demo.springboot.service.impl.AverageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api")
public class AverageApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(AverageApi.class);
    @Autowired
    AverageService averageService;
    @RequestMapping(value = "/averagewired/{srednia}", method = RequestMethod.GET)
    public ResponseEntity<AverageData> testDocument1(@PathVariable("srednia") String srednia){
        AverageData averageData = new AverageData(averageService.getAverage(srednia));
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", averageData);

        return new ResponseEntity<>(averageData, HttpStatus.OK);
    }
    @Autowired
    AverageService averageService2;
    @RequestMapping(value = "/averagewired", method = RequestMethod.GET)
    public ResponseEntity<AverageData> testDocument2(@RequestParam(value = "srednia", required = false) String srednia){
        AverageData averageData = new AverageData(averageService.getAverage(srednia));
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", averageData);

        return new ResponseEntity<>(averageData, HttpStatus.OK);
    }
}
