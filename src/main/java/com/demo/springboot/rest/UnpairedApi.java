package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MinData;
import com.demo.springboot.domain.dto.UnpairedData;
import com.demo.springboot.service.impl.MinService;
import com.demo.springboot.service.impl.UnpairedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api")
public class UnpairedApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(UnpairedApi.class);

    @Autowired
    UnpairedService unpairedService;
    @RequestMapping(value = "/unpaired/{digits}", method = RequestMethod.GET)
    public ResponseEntity<UnpairedData> testDocument1(@PathVariable("digits") String digits) throws Exception {
        UnpairedData unpairedData = new UnpairedData(unpairedService.getUnpaired(digits));
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", unpairedData);

        return new ResponseEntity<>(unpairedData, HttpStatus.OK);
    }
}
