package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.SortData;
import com.demo.springboot.service.impl.SortService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api")
public class SortApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(SortApi.class);

    @Autowired
    SortService sortService;
    @RequestMapping(value = "/sort/{digits}", method = RequestMethod.GET)
    public ResponseEntity<SortData> testDocument1(@PathVariable("digits") String digits) {
        SortData sortData = new SortData(sortService.getSorted(digits));
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", sortData);

        return new ResponseEntity<>(sortData, HttpStatus.OK);
    }
}
