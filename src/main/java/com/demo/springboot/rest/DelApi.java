package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.DelCharData;
import com.demo.springboot.service.impl.DelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api")
public class DelApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(DelApi.class);

    @Autowired
    DelService delService;
    @RequestMapping(value = "/delwired", method = RequestMethod.GET)
    public ResponseEntity<DelCharData> testDocument2(@RequestParam(value = "text", required = true) String text,
                                                 @RequestParam(value = "character", required = true) char character){
        DelCharData delCharData = new DelCharData(delService.getLine(text, character));
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", delCharData);

        return new ResponseEntity<>(delCharData, HttpStatus.OK);
    }
}
