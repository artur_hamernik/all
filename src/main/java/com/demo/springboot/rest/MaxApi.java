package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.AverageData;
import com.demo.springboot.domain.dto.MaxData;
import com.demo.springboot.service.impl.AverageService;
import com.demo.springboot.service.impl.MaxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api")
public class MaxApi {
        private static final Logger LOGGER = LoggerFactory.getLogger(MaxApi.class);

        @Autowired
        MaxService maxService;
        @RequestMapping(value = "/maxwired/{max}", method = RequestMethod.GET)
        public ResponseEntity<MaxData> testDocument1(@PathVariable("max") String max){
            MaxData maxData = new MaxData(maxService.getMax(max));
            LOGGER.info("######### testDocuemnt działa!!!!##########");
            LOGGER.info("######### Srednia wynosi {} ", maxData);

            return new ResponseEntity<>(maxData, HttpStatus.OK);
        }
        @RequestMapping(value = "/maxwired", method = RequestMethod.GET)
        public ResponseEntity<MaxData> testDocument2(@RequestParam(value = "max", required = true) String max){
            MaxData maxData = new MaxData(maxService.getMax(max));
            LOGGER.info("######### testDocuemnt działa!!!!##########");
            LOGGER.info("######### Srednia wynosi {} ", maxData);

            return new ResponseEntity<>(maxData, HttpStatus.OK);
        }
    }
