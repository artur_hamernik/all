package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.AverageData;
import com.demo.springboot.domain.dto.MaxData;
import com.demo.springboot.domain.dto.MinData;
import com.demo.springboot.service.impl.AverageService;
import com.demo.springboot.service.impl.MaxService;
import com.demo.springboot.service.impl.MinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Min;

@Controller
@RequestMapping("/api")
public class MinApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(MinApi.class);

    @Autowired
    MinService minService;
    @RequestMapping(value = "/minwired/{min}", method = RequestMethod.GET)
    public ResponseEntity<MinData> testDocument1(@PathVariable("min") String min){
        MinData minData = new MinData(minService.getMin(min));
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", minData);

        return new ResponseEntity<>(minData, HttpStatus.OK);
    }
    @RequestMapping(value = "/minwired", method = RequestMethod.GET)
    public ResponseEntity<MinData> testDocument2(@RequestParam(value = "min", required = true) String min){
        MinData minData = new MinData(minService.getMin(min));
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", minData);

        return new ResponseEntity<>(minData, HttpStatus.OK);
    }
}
